from django.urls import path
from .views import index, ret_json

urlpatterns = [
    path('', index, name='index'),
    path('returnJSON', ret_json, name='ret_json')
]