from django.shortcuts import render
from django.http.response import HttpResponse, JsonResponse
from django.core import serializers
from lab_2.models import Note
import json

# Create your views here.

def index(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab5_index.html', response)

def ret_json(request):
    notes =  list(Note.objects.all().values())
    daftar_notes = json.dumps(notes)
    return JsonResponse(daftar_notes, safe=False)