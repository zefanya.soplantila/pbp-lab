- Nama = Zefanya Soplantila
- NPM = 2006597696
<br>
1. Apakah perbedaan antara JSON dan XML?
    * JSON = suatu pengolahan untuk menampilkan data yang tergantung pada sebuah bahasa pemrograman yaitu javascript.
    * XML = suatu pengolahan data yang ditampilkan dengan struktur tag dari markup language sehingga struktur ini dapat dibaca oleh mesin (tidak tergantung bahasa pemrograman) dan manusia.


2. Apakah perbedaan antara HTML dan XML?
    * HTML => Mendeskripsikan secara keseluruhan struktur dari halaman web serta informasi yang ada.
    * XML => Mendeskripsikan struktur penyimpanan dan transfer informasi dan menjelaskan apa jenis data yang di proses
