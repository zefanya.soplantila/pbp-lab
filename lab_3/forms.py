from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'DOB']

        error_messages = {
            'required' : 'Please type'
        }

        input_attrs = {
            'type' : 'text',
            'placeholder' : 'Masukkan Nama',
        }

        input_attrs2 = {
            'type' : 'number',
            'placeholder' : 'Masukkan NPM Anda',
        }

        input_attrs3 = {
            'type' : 'date'
        }

        name = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs)) 
        npm = forms.CharField(label='', required=True, max_length=50, widget=forms.NumberInput(attrs=input_attrs2))
        DOB = forms.CharField(label='', required=True, widget=forms.DateInput(attrs=input_attrs3))
