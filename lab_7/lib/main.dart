import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title: "Soplanz Form",
    home : MyForm()
  ));
}

class MyForm extends StatefulWidget{
  @override
  _MyFormState createState() => _MyFormState();
}

class _MyFormState extends State<MyForm>{
  final _formKey = GlobalKey<FormState>();
  
  double age = 1;
  String name = "";
  String email= "";
  
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("My Form"),
        centerTitle: true
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(35.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      labelText: "Name",
                      hintText : "Enter Your Name (eg: David Silva)",
                      icon: Icon(Icons.people)
                    ),
                    validator: (String? value){
                      if(value == null){
                        return "Name cannot be empty";
                      }
                      name = value.toString();
                      return null;
                    }
                  )
                ),
                const SizedBox(
                  height: 10.0
                ),
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      labelText: "Email",
                      hintText : "Enter Your Email (eg: DavidSilva@ui.ac.id)",
                      icon: Icon(Icons.contacts)
                    ),
                    validator: (String? value){
                      if(value == null){
                        return "Email cannot be empty";
                      }
                      email = value.toString();
                      return null;
                    }
                  )
                ),
                const SizedBox(
                  height: 10.0
                ),
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: Column(
                    children: [
                      const Text("Age"),
                      Slider(
                        value: age,
                        min: 1,
                        max: 100,
                        onChanged: (value){
                          setState((){
                            age = value;
                          });
                        }
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
                        child: Text("Your age: " + age.toInt().toString()),
                      )
                    ]
                  ) 
                ),
                ElevatedButton(
                  child: const Text("Submit Data"),
                  onPressed: (){
                    if(_formKey.currentState!.validate()){
                      showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          title: const Text("Data Submitted Successfully :D"),
                          content: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Text("Your Name  : " + name)
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Text("Your Email : " + email)
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Text("Your Age   : " + age.toInt().toString())
                                  ),
                                  ]
                                ),
                              ),
                          actions: <Widget>[
                            ElevatedButton(
                              child: const Text("Okay"),
                              onPressed: () => Navigator.pop(context, "Okay")
                            )
                          ]
                        )
                      );
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.deepOrange,
                    onSurface: Colors.white12,
                  )
                )
              ]
            )
          )
        )
      )
    );
  }
}

