from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):

    class Meta:
        model = Note
        fields = ['to', 'froms', 'title', 'message']

        input_attrs = {
            'type' : 'text',
            'placeholder' : 'Nama Penerima',
        }

        input_attrs2 = {
            'type' : 'text',
            'placeholder' : 'Nama Pengirim',
        }

        input_attrs3 = {
            'type' : 'text',
            'placeholder' : 'Judul',
        }

        input_attrs4 = {
            'type' : 'text',
            'placeholder' : 'Masukkan Pesan',
        }

        to = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs)) 
        froms = forms.CharField(label='', required=True, max_length=50, widget=forms.NumberInput(attrs=input_attrs2))
        title = forms.CharField(label='', required=True, max_length=100, widget=forms.TextInput(attrs=input_attrs3))
        message = forms.CharField(label='', required=True, max_length=1500, widget=forms.TextInput(attrs=input_attrs4))
