from django.shortcuts import render, HttpResponseRedirect
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.

def index(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab4_index.html', response)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab4_note_list.html', response)

def add_note(request):
    context = {}
    form = NoteForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-4')
    else:
        context['form'] = form
        return render(request, 'lab4_form.html', context)